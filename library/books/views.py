from django.shortcuts import render
from django.views.generic.base import View

from .models import Book, Shelf



class BooksView(View):
    """Список книг"""
    def get(self, request):
        books = Book.objects.all()
        return render(request, "books/book_list.html", {"book_list": books})
    

class GetBookById(View):
    """Получение книги по id"""
    def get(self, request, pk):
        book = Book.objects.get(name_id=pk)
        return render(request, "books/book_info_by_id.html", {'book' : book})


class GetShelfList(View):
    """Получение информации о стеллаже по его id"""
    def get(self, request):
        shelfs = Shelf.objects.all()
        return render(request, "shelfs/GetShelfList.html", {'shelfs' : shelfs})