from django.contrib import admin
from .models import Shelf, Book



admin.site.register(Shelf)
admin.site.register(Book)
