from django.urls import path
from . import views


urlpatterns = [
    path("", views.BooksView.as_view()),
    path("books/<int:pk>/", views.GetBookById.as_view()),
    path("shelfs/", views.GetShelfList.as_view()),
]

