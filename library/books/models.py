from django.db import models




class Shelf(models.Model):
    """Стеллажи"""
    name = models.CharField("Стеллаж", max_length=150)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = "Стеллаж"
        verbose_name_plural = "Стеллажи"


class Book(models.Model):
    """Книги"""
    name = models.CharField("Название", max_length = 150)
    name_id = models.IntegerField("id", unique=True)
    full_author_name = models.CharField("ФИО автора", max_length = 150)
    short_description = models.TextField("Краткое содержание")
    status = models.BooleanField("Взята ли книга")
    shelf = models.ForeignKey(
        Shelf, verbose_name='shelf', related_name='books', on_delete=models.SET_NULL, null=True
    )
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = "Книга"
        verbose_name_plural = "Книги"
    